﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class FollowTag<T> : MonoBehaviour where T: TagMono {

    public bool m_mirrorPosition=true;
    public bool m_mirrorRotation = true;
    public bool m_mirrorScale = true;

    [Header("Debug")]
    public T m_toFollow;

    void SetObjectToFollow(T toFollow) {
        m_toFollow = toFollow;
    }

    private void Awake()
    {
        m_toFollow = GetCurrentType();
    }

    protected abstract T GetCurrentType();

    private void LateUpdate()
    {

        if (m_toFollow == null)
            m_toFollow = GetCurrentType();

        if (m_toFollow)
        {
            if (m_mirrorPosition)
                transform.position = m_toFollow.GetTransform().position;
            if (m_mirrorRotation)
                transform.rotation = m_toFollow.GetTransform().rotation;
            if (m_mirrorScale)
                transform.localScale = m_toFollow.GetTransform().localScale;

        }
    }
    
}
