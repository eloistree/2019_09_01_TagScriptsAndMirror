﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightEyeTag : TagMono<RightEyeTag>
{

    protected override RightEyeTag GetCurrent()
    {
        return this;
    }

}

