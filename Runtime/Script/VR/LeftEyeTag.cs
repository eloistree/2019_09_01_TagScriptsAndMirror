﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftEyeTag : TagMono<LeftEyeTag>
{

    protected override LeftEyeTag GetCurrent()
    {
        return this;
    }
}

