﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowLeftHandTag : FollowTag<LeftHandTag>
{
    protected override LeftHandTag GetCurrentType()
    {
        return LeftHandTag.GetStaticTagCollection().GetLastInstanceInScene();
    }
}
