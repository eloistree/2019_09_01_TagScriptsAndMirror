﻿
public class FollowHeadTag : FollowTag<HeadTag>
{
    protected override HeadTag GetCurrentType()
    {
        return HeadTag.GetStaticTagCollection().GetLastInstanceInScene();
    }
}