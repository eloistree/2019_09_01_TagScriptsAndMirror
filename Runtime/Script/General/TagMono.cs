﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITaggedScript {
    GameObject GetGameObject();
    Transform GetTransform();
    Rigidbody GetRigidbody();
}

public class TagMono : MonoBehaviour , ITaggedScript
{
    public GameObject GetGameObject()
    {
        if (m_gameobject == null)
            m_gameobject = gameObject;
        return m_gameobject;
    }

    public Transform GetTransform()
    {
        if (m_transform == null)
            m_transform = transform;

        return m_transform;
    }

    public Rigidbody GetRigidbody()
    {
        if (m_rigidbody == null)
            m_rigidbody =  GetComponent<Rigidbody>();

        return m_rigidbody;
    }

    Transform m_transform;
    Rigidbody m_rigidbody;
    GameObject m_gameobject;
}

public abstract class TagMono<T> : TagMono where T: MonoBehaviour
{
    private static TagCollection<T> tagcollection = new TagCollection<T>();
    public static TagCollection<T> GetStaticTagCollection() { return tagcollection; }
    public  TagCollection<T> GetTagCollection() {
        return tagcollection;
    }




    private void OnEnable()
    {
        GetTagCollection().NotifyNewValue(GetCurrent());
    }

    protected abstract T GetCurrent();

    private void OnDisable()
    {
        GetTagCollection().NotifyRemovedValue(GetCurrent());
    }


    public void Update()
    {
        if (m_useUpdateToRefresh)
            RefreshDebug();
    }


    [Header("Debug information")]
    [SerializeField] bool m_refresh;
    [SerializeField] bool m_useUpdateToRefresh;
    [SerializeField] T m_first;
    [SerializeField] T m_last;
    [SerializeField] T[] m_all;

    protected void OnValidate()
    {
        m_refresh = false;
        RefreshDebug();
    }

    private void RefreshDebug()
    {
        m_first = GetTagCollection().m_firstInstanceInTheScene;
        m_last = GetTagCollection().m_lastInstanceInTheScene;
        m_all = GetTagCollection().GetAllInstances();
    }

}
