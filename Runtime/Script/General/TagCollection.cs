﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TagCollection {

//    public static Dictionary<string, TagCollection> m_registered = new Dictionary<string, TagCollection>();



}


public class TagCollection<T> : TagCollection where T : MonoBehaviour
{
   


    public T m_firstInstanceInTheScene;
    public T m_lastInstanceInTheScene;
    private List<T> m_collection = new List<T>();


    //public TagCollection() {

    //    string typeId = GetType().ToString();
    //    if (!TagCollection.m_registered.ContainsKey(typeId))
    //        TagCollection.m_registered.Add(typeId, this);
    //    Debug.Log(GetType());
    //}
    

    public void NotifyNewValue(T value)
    {
        
        if (m_firstInstanceInTheScene == null)
            m_firstInstanceInTheScene = value;
        m_lastInstanceInTheScene = value;
        Add(value);

    }

    public void NotifyRemovedValue(T value)
    {

        if (m_firstInstanceInTheScene == value)
            m_firstInstanceInTheScene = GetRandomInstanceInTheScene();

        if (m_lastInstanceInTheScene == value)
            m_lastInstanceInTheScene = GetRandomInstanceInTheScene();
        Remove(value);
    }

    internal T[] GetAllInstances()
    {
        return m_collection.ToArray();
    }

    public int GetNumberOfInstance() { return m_collection.Count; }
    public bool HasInstanceInScene() { return GetNumberOfInstance() > 0; }
    public bool IsSingleInstanceInScene() { return GetNumberOfInstance() == 1; }


    public T GetRandomInstanceInTheScene()
    {
        m_collection.Remove(null);
        if (m_collection.Count <= 0)
            return null;
        return m_collection[UnityEngine.Random.Range(0, m_collection.Count)];
    }

    private void Add(T value)
    {
        if (!m_collection.Contains(value))
            m_collection.Add(value);
    }
    private void Remove(T value)
    {
        m_collection.Remove(value);
    }

    public T GetLastInstanceInScene()
    {
        return m_lastInstanceInTheScene;
    }
    public T GetFirstInstanceInTheScene ()
    {
        return m_firstInstanceInTheScene;
    }

}