﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRightHandTag : FollowTag<RightHandTag>
{
    protected override RightHandTag GetCurrentType()
    {
        return RightHandTag.GetStaticTagCollection().GetLastInstanceInScene();
    }
}
